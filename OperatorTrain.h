#pragma once
#include <iostream>
using namespace std;
class OperatorCal
{
public:
    OperatorCal(double a, double b);
    // opereator키워드와 공백업이 연결표시 , 간편하게 이름을 정의 가능함
    // 기존에 존재하던 연산자의 기능에서 추가하는 것뿐읻.
    //nb1 + nb2 = nb1.operator+(nb2) 같은 이야기임

    // 전역함수     
    friend OperatorCal operator+(const OperatorCal& pos1, const OperatorCal& pos2);
    friend OperatorCal operator+(double num, const OperatorCal& pos);

    /*
    1.friend 키워드는 함수의 원형에서만 사용해야 하며, 함수의 정의에서는 사용하지 않습니다.
    2.이렇게 선언된 프렌드 함수는 클래스 선언부에 그 원형이 포함되지만, 클래스의 멤버 함수는 아닙니다
    */

    // 클래스멤버 함수 
    OperatorCal operator+(const OperatorCal& pos1);
    OperatorCal operator+(int num) const;
    void print();
private:
    double a_;
    double b_;

};