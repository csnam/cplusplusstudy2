#include <iostream>
using namespace std;
class Position
{
private:
	double x_;
	double y_;
public:
	Position(double x, double y);	// 생성자 
	void Display();
	friend Position operator-(const Position& pos1, const Position& pos2);	// - 연산자 함수 
};

//int main(void)
//{
//	Position pos1 = Position(3.3, 12.5);
//	Position pos2 = Position(-14.4, 7.8);
//	Position pos3 = pos1 - pos2;
//
//	pos3.Display();
//	return 0;
//}

Position::Position(double x, double y)
{
	x_ = x;
	y_ = y;
}

Position operator-(const Position& pos1, const Position& pos2)
{
	return Position((pos1.x_ + pos2.x_) / 2, (pos1.y_ + pos2.y_) / 2);
}

void Position::Display()
{
	cout << "두 지점의 중간 지점의 좌표는 x좌표가 " << this->x_ << "이고, y좌표가 " << this->y_ << "입니다." << endl;
}



class Rect
{
private:
	double height_;
	double width_;
public:
	Rect(double height, double width);	// 생성자 
	void DisplaySize();
	Rect operator*(double mul) const;
	friend Rect operator*(double mul, const Rect& origin);	// 프렌드 함수 
};

int main_this(void)
{
	Rect origin_rect(10, 20);
	Rect rect01 = origin_rect * 2;
	Rect rect02 = 3 * origin_rect;

	rect01.DisplaySize();
	rect02.DisplaySize();
	return 0;
}

Rect::Rect(double height, double width)
{
	height_ = height;
	width_ = width;
}

void Rect::DisplaySize()
{
	cout << "이 사각형의 높이는 " << this->height_ << "이고, 너비는 " << this->width_ << "입니다." << endl;
}

Rect Rect::operator*(double mul) const
{
	return Rect(height_ * mul, width_ * mul);
}

Rect operator*(double mul, const Rect& origin)
{
	return origin * mul;
}