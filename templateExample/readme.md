클래스 템플릿
1 선언
```
template<typename 일반화유형> class  클래스이름
{
};

template<typename T> class ABC 
{
};
```
2.정의
```
template<typename 일반화유형> 리턴형 클래스이름 <일반화유형>::멤버함수(매개변수)
{
}
template<typename T> void ABC<T>::runtest(int a)
{
}
```
3.사용
```
클래스 이름 < 대치될 자료형>  객체이름
ABC<int> abc
```

4. 클래스 선언와 정의가 하나의 파일에 있어야함.

5. 참조:  https://www.youtube.com/watch?v=UisD-T9erEk