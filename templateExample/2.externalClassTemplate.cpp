// member-function_tempate
template<class T,int i> class MyClass
{
	T* pMyclass;
	T  buffer[i];
	static const int cItems = i * sizeof(T);
public:
	MyClass(void); // contructor
	void push(const T item);
	T& pop(void);

};
template<class T, int i> MyClass<T, i>::MyClass(void)
{

}

template<class T, int i> void MyClass<T, i>::push(const T item)
{

}
template<class T, int i> T& MyClass<T, i>::pop(void)
{


}
//-----------------------
// membear template
//------------------------
template<typename T> class ABC
{
	template<typename U> void run(const U& u);
};

template<typename T> template<typename U> void ABC<T>::run(const U& u)
{

}


