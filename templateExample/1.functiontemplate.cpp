#include <iostream>

template <typename T> T ADD(T a, T b);


template <typename T> T ADD(T a, T b)
{
	return a + b;
}

int main_1()
{
	int a = 3, b = 4;
	double c = 3.5, d = 4.112;
	std::cout << "T:" << ADD(a, b) << std::endl;
	std::cout << "T:" << ADD(c, d);
	return 0;
}
