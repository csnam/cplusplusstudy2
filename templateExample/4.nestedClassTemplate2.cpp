#include <iostream>
using namespace std;
template <class T> class ABC
{
	template <class U> class DEF
	{
		U* u;
	public:
		DEF();
		~DEF();

		U& Value();
		void print();
		
	};
	
	DEF<int> def;
public:
	ABC(T t)
	{
		def.Value() = t;
	}
	void print()
	{
		def.print();
	}
};
template<class T> template<class U > ABC<T>::DEF<U>::DEF()
{
	u = new U();
}
template<class T> template<class U> ABC<T>::DEF<U>::~DEF()
{
	delete u;
}
template<class T> template<class U> U& ABC<T>::DEF<U>::Value()
{
	return *u; //???
}

template<class T> template<class U> void ABC<T>::DEF<U>::print()
{
	cout << this->Value() << endl;
}

int main()
{
	/*
	ABC<int>* xi = new ABC<int>(10);  //ABC(T t) 
		xi->print();
	/*/
	  ABC<int> abc(10);
	  abc.print();
    //*/
	  	  

	ABC<char>* xc = new ABC<char>('c');
	xc->print();

	ABC<char> abc1('c');
	abc1.print();

	return 0;
}