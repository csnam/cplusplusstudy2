//Nested class template
#include <iostream>
using namespace std;

class ABC
{
private:
	template <class T> struct DEF {
		T m_t;
		DEF(T t):m_t(t){}  //???  생성자 초기화 리스트
	};
	DEF<int> yInt;
	DEF<char>ychar;
public:
	ABC(int i, char c) : yInt(i), ychar(c)
	{

	}
	void print()
	{
		cout << "START=====" << endl;
		cout << yInt.m_t << " * " << ychar.m_t << endl;
		cout << "END=====" << endl;
	}

};
void main_3()
{
	ABC  abc(1, 'a');
	abc.print();
}

/* 클래스 템플릿
1  선언
template<typename 일반화유형> class  클래스이름
{
};

template<typename T> class ABC 
{
};
//2  정의
template<typename 일반화유형> 리턴형 클래스이름 <일반화유형>::멤버함수(매개변수)
{
}

template<typename T> void ABC<T>::runtest(int a)
{
}
// 사용
클래스 이름 < 대치될 자료형>  객체이름
 ABC<int> abc


-------------*/


