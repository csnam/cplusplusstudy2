﻿
#include <iostream>
#include "OperatorTrain.h"



class lamdaTest
{
public:
    void print()
    {
        std::cout << "res:" << res << std::endl;
        std::cout << "a: " << a1 << "b: " << b1 << "c :" << c1 << "d: " << d1;
    }
    /*

    //참조
    https://en.cppreference.com/w/cpp/language/lambda
    https://docs.microsoft.com/ko-kr/cpp/cpp/lambda-expressions-in-cpp?view=vs-2019
    */
    
    void basic_lamda_heck()//basic lamda function
    {
        //basic lamda express 
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;

        //[capture] (param )-> ret type { body statement } ( 
        // 

        res = [](int c, int d)->int { return c + d; }(a, b);

        //capture절에서 a를 값을 사용 
        res = [a](int c, int d)->int { return a + c + d; }(a, b);  //

        //capture절에서 모든 변수를 값을 사용
        res = [=](int c, int d)->int { return a + b + c + d; }(a, b);  //

        //capture절에서 a를 참조을 사용
        res = [&a](int c, int d)->int { a = 5; return a + c + d; }(a, b);  //

        
        //capture절에서 모든 변수를 참조로 사용
        res = [&](int c, int d)->int { a = 6; b = 7; return a + b + c + d; }(a, b);  //

        // a 참조  b:값으로 사용
        res = [&, b](int c, int d)->int { a = 6; return a + b + c + d; }(a, b);  //

        // ret type이 void 경우   -> int 생략
        [&, b](int c, int d) { a = 8; return; }(a, b);

        // [] ()  ret  생략가능 
        [&] {a = 9; std::cout << "a: " << a; };  // 선언만 한 경우임 실행 안됨
        
        [&] {a = 9; std::cout << "a: " << a; }();  //선언 & 실행   값이 변동 
        
        {std::cout << "end: " << a <<" "<< b << std::endl; }(a, b);  //

        a1 = a; b1 = b; c1 = c; d1 = d;
    }
private:
        
    int res;    
    int a1, b1, c1, d1;


};

int main()
{
    // 람다 테스트  3
    //lamdaTest lamdaTest;
    //lamdaTest.basic_lamda_heck();
    //lamdaTest.print();

    OperatorCal opCal(3, 5);
    OperatorCal sumCam(0, 0);
    OperatorCal opCal2(1, 1);
    sumCam = opCal + opCal2;
    sumCam + sumCam;
    sumCam.print(); 

    
}






