
#include <iostream>
int& foo() 
{
	int a  = 10;
	return a;
}
int aa = 9;

int foobar()
{
	return 5;
}

int main_6()
{
// left value
	
	int* pVal_1 = &aa;
	
	
	std::cout << "foo(): " << pVal_1 << std::endl;
	std::cout << "foo()내용: " << *pVal_1 << std::endl;
	
	foo() = 43;
	int* pVal = &foo();
	std::cout << "foo(): " << pVal << std::endl;


// R value
	int j = 0;
	j = foobar();
	std::cout << "foobar(): " << j << std::endl;
	//int* p = &foobar();    //'&'에 l-value가 있어야 합니다.


	/*
	X&& right vlaue reference

	void foo(X& x);   // 좌측값 참조 오버로드
	void foo(X&& x);  // 우측값 참조 오버로드

	*/

	//인자로 받은 것을 우측값으로 바꿔주는 역할을 합니다.


	
	return 0;
		
}

