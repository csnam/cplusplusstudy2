#include <functional>
#include <iostream>
#include <functional>
#include <iostream>

class ABC {
public:
	int data_;
	ABC(int data) :data_(data) 
	{
		std::cout << "일반 생성자 호출!" << std::endl;
	}
	ABC(const ABC& abc) {
		std::cout << "복사 생성자 호출!" << std::endl;
		data_ = abc.data_;
	}
	/*ABC(ABC&& abc)
	{
		std::cout << "이동 생성자 호출!" << std::endl;
		data_ = abc.data_;
	}*/

};

void do_func(ABC& a1, const ABC& b1) { a1.data_ = b1.data_ + 10; }

int main_5()
{
	ABC  a(1), b(2);
	auto do_func_with_s1 = std::bind(do_func, a, std::placeholders::_1);
	
	std::cout << " before a: " << a.data_ << std::endl;
	do_func_with_s1(b);
	std::cout << " after  a: " << a.data_ << std::endl;;


	auto do_func_with_s1_ref = std::bind(do_func, std::ref(a), std::placeholders::_1);

	std::cout << " before2 a: " << a.data_ << std::endl;
	do_func_with_s1_ref(b);
	std::cout << " after2  a: " << a.data_ << std::endl;;
	return 0;
}