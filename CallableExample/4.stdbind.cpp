#include <iostream>
#include <functional>
void add(int x, int y) {
    std::cout << x << " + " << y << " = " << x + y << std::endl;
}

void subtract(int x, int y) {
    std::cout << x << " - " << y << " = " << x - y << std::endl;
}


int main_4()
{
    auto add_with2 = std::bind(add, 2, std::placeholders::_1);
    add_with2(3);
    add_with2(4, 5);

    auto subtract_with2 = std::bind(subtract, std::placeholders::_1, 2);
    auto netgate = std::bind(subtract, std::placeholders::_2, std::placeholders::_1);
    subtract_with2(10);
    netgate(3, 10);  //10-3


    return 0;
}


