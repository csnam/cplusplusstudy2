#include <iostream>
#include <functional>
struct SS
{    void operator()(int a, int b) { std::cout << "a+b = " << a + b << std::endl; };
}; // ; 주의 



int main_0()
{
	SS ss;	
	ss(4,6);

	// 람다함수  [capture] (param)-> ret-type {  body }

	auto f = [](int a, int b) -> int { std::cout << "a+b = " << a + b << std::endl; return 1; };
	f(4, 7);

	//[] 생략가능 ->
	int a =4 , b = 8;
	{ std::cout << "lamda2 a+b = " << a + b << std::endl; } (4, 7);

	

	return 0;
}

 