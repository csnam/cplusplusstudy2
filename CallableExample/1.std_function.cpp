#include <iostream>
#include <functional>
#include <string>


int do_function(const std::string& arg)
{
	std::cout << "dofunc: " << arg;
	return 0;
}
struct ABC
{
	void operator()(char c) { std::cout << "호출:" << c << std::endl; };
}; 

class DEF
{
private:
    int a_;
public:
    DEF(int a) : a_(a) {};
	int do_func() { std::cout << "do func: " << a_ << std::endl; return a_; };

};

int main_1()
{
	std::function<int(const std::string& arg)> f1 = do_function;
	std::function<void(char)> f2 = ABC();
	std::function<void(char)> f3 = [](char c) {std::cout << "calller: " << c << std::endl; };
	
	f1("hello");
	f2('c');
	f3('d');

	DEF aa(5);
	//std::function<int()> f4 = aa.do_func;  // bad express 

	std::function<int(DEF&)> f4 = &DEF::do_func; //명시적으로 주소연산자 사용
	
	f4(aa);  //** 객체를 넣으면 객체의 함수의 멤버함수를 호출하는 것과 동일한 효과가 있다.
	return 0;
}

// transform


//unary_op함수 :
//Ret unary(const Type& a);