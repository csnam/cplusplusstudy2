### 참조형 변수
1. : 선언과 동시에 초기화 해야한다.
일반변수, 포인터 변수, 참조형변수
 ```
	int value =5 
    int& ref = value
```
2. null값 참조없다, const, r-value로 초기화 불가능 
3. 초기화 한후 다른 변수 참조 불가
4. 함수 매개 변수로 많을 사용. 복사본이 많들어지지 않는다.
  포인터 인수 전달 * 역참조로 변수값 수정
  참조형 매개변수는 역참조 없이 수정 
5.  참조 vs 포인터
```
int value=5;
int* ptr = &value;
int& ref = value; 
*ptr=6;
ref =6;
```
같은 의미

6.참조형은 선언과 동시에 객체로 초기화 해야하고 초기화하기에 포인터보다 사용이 안전하다.


## R-Value Reference

1. C: assignment(왼쪽)    epress (오른쪽)
C++ :
1. L-value 왼쪽: 메모리위치를 가리키는 데 & 연산자를 통해 그 위치 참조가능하다.
```
int& foo();
foo()= 42;
int* p1 = *foo();
```
1. R-value : l-value 이외의 모든값

