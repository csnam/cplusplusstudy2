#include "OperatorTrain.h"

// 클래스 멤버로 정의하는 방법 
OperatorCal OperatorCal::operator+(const OperatorCal& pos1)
{
    return OperatorCal(a_ + pos1.a_, b_ + pos1.b_);

}
OperatorCal OperatorCal::operator+(int num) const
{
    return OperatorCal(a_ + num, b_ + num);

}
//전역함수로 정의 
//전역 함수는 private 멤버인 a와 b_에 접근하지 못하므로, friend 키워드를 사용하여 프렌드 함수
OperatorCal operator+(const OperatorCal& pos1, const OperatorCal& pos2)
{
    return OperatorCal((pos1.a_ + pos2.a_) / 2, (pos1.b_ + pos2.b_) / 2);

}
OperatorCal operator+(double num, const OperatorCal& pos)
{
    return OperatorCal(pos.a_ + num, pos.b_ + num);
    //return pos. + num;

}

// 초기화 리스트 
OperatorCal::OperatorCal(double a, double b):a_(a),b_(b)
{
    //a_ = a; b_ = b; 
}
void OperatorCal::print()
{
    std::cout << "a:" <<a_ << "b:" <<b_;
}
